export const ABOUT = 'about';
export const REGISTER = 'register';
export const LOGIN = 'login';
export const LOGOUT = 'logout';
export const HOME = 'home';
export const BAND = 'bands';
export const SINGLEBAND = 'singleBand'