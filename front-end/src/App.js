import { Component } from "react";
import './App.css';
import { ABOUT, REGISTER, LOGIN, HOME, LOGOUT, BAND, SINGLEBAND } from "./Utils/Constants";
import HomeView from "./CustomComponents/HomeView";
import AboutView from "./CustomComponents/AboutView";
import RegisterView from "./CustomComponents/RegisterView";
import LoginView from "./CustomComponents/LoginView";
import BandView from "./CustomComponents/BandView";
import SingleBandView from "./CustomComponents/SingleBandView";
import AddMemberView from "./CustomComponents/AddMemberView";
import AddBandView from "./CustomComponents/AddBandView";
import axios from "axios";
import { API_URL } from "./Utils/Configuration";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      CurrentPage: HOME,
      user: null,
      showAddBandView: false,
      showSingleBandView: false,
      selectedBand: null,
      showAddMemberView: false
    };
  }

  componentDidMount() {
    this.checkSession();
  }

  checkSession = () => {
    axios.get(API_URL + '/users/session', { withCredentials: true })
      .then(response => {
        if (response.data && response.data.logged_in) {
          this.setState({ user: response.data.user });
        }
      })
      .catch(err => {
        console.log(err);
        this.setState({ user: null });
      });
  }
  
  toggleAddBandView = () => {
    this.setState(prevState => ({ showAddBandView: !prevState.showAddBandView }));
  }
  
  toggleSingleBandView = (band) => {
    this.setState({ selectedBand: band, showSingleBandView: true, showAddMemberView: false });
  }

  toggleAddMemberView = (band) => {
    if (band) {
      this.setState({ selectedBand: band, showAddMemberView: true, showSingleBandView: false });
    } else {
      console.error("Selected band is not set");
    }
  }

  handleMemberAdded = () => {
    this.setState({ showAddMemberView: false, showSingleBandView: true });
  }

  handleBandAdded = () => {
    this.setState({ showAddBandView: false });
  };

  handleRegistrationSuccess = () => {
    this.setState({
      CurrentPage: LOGIN,
      registrationSuccessMessage: "You registered successfully, now log in"
    });
  }

  handleLoginSuccess = () => {
    this.setState({
      CurrentPage: HOME
    });
  }

  setSelectedBand = (band) => {
    this.setState({ selectedBand: band, showSingleBandView: true });
  }

  QGetView = () => {
    const { CurrentPage, user, showAddBandView, showSingleBandView, selectedBand, showAddMemberView } = this.state;

    if (showAddMemberView) {
      return (
        <AddMemberView
          id_band={this.state.selectedBand ? this.state.selectedBand.id_band : null}
          id_user={user ? user.id_user : null}
          onMemberAdded={this.handleMemberAdded}
        />
      );
    }
    if (showAddBandView) {
      return (
        <AddBandView
          id_user={user ? user.id_user : null}
          onBandAdded={this.handleBandAdded}
        />
      );
    }
    if (showSingleBandView) {
      return <SingleBandView band={selectedBand} id_user={user ? user.id_user : null} isArtist={user && user.isArtist === 1}
        onAddMember={(band) => this.toggleAddMemberView(band)} />;
    }

    switch (CurrentPage) {
      case ABOUT:
        return <AboutView />;
      case REGISTER:
        return <RegisterView onRegisterSuccess={this.handleRegistrationSuccess} />;
      case LOGIN:
        return <LoginView
          QUserFromChild={this.QSetLoggedIn}
          registrationSuccessMessage={this.state.registrationSuccessMessage}
          onLoginSuccess={this.handleLoginSuccess} />;
      case LOGOUT:
        return <HomeView />;
      case BAND:
        return (
          <BandView
            isArtist={user && user.isArtist === 1}
            onAddBandClick={this.toggleAddBandView}
            onViewDetailsClick={(band) => this.toggleSingleBandView(band)} />
        );

      default:
        return <HomeView user={this.state.user}/>;
    }
  };


  QSetUser = (user) => {
    this.setState({ user });
  }

  QPostLogout = () => {
    axios.get(API_URL + '/users/logout', { withCredentials: true })
      .then(response => {
        if (response.status === 200) {
          this.setState({ user: null, CurrentPage: HOME });
        } else {
          console.log("Logout failed, DEBUG!");
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  QSetView = (page) => {
    this.setState({
      CurrentPage: page.page,
      showAddBandView: false,
      showSingleBandView: false,
      showAddMemberView: false
    });
  };

  QSetLoggedIn = (user, loginSuccessful) => {
    if (loginSuccessful) {
      this.setState({ user, CurrentPage: HOME });
    } else {
      this.setState({ user });
    }
  }


  render() {
    const { user } = this.state;
    return (
      <div id="APP" className="container">
        <div id="menu" className="row">
          <nav className="navbar navbar-expand-lg navbar-dark custom-navbar">
            <div className="container-fluid">
              <a
                onClick={(e) => { e.preventDefault(); this.QSetView({ page: "home" }); }}
                className="navbar-brand"
                href="#"
              >
                DiscoverMe
              </a>
              <button
                className="navbar-toggler"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="navbar-toggler-icon"></span>
              </button>

              <div
                className="collapse navbar-collapse"
                id="navbarSupportedContent"
              >
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                  <li className="nav-item">
                    <a onClick={(e) => { e.preventDefault(); this.QSetView({ page: ABOUT }); }} className="nav-link" href="#">
                      About
                    </a>
                  </li>
                  {
                    this.state.user == null && (
                      <li className="nav-item">
                        <a onClick={(e) => { e.preventDefault(); this.QSetView({ page: REGISTER }); }} className="nav-link" href="#">
                          Register
                        </a>
                      </li>
                    )
                  }

                  {user != null &&
                    <li className="nav-item">
                      <a onClick={(e) => { e.preventDefault(); this.QSetView({ page: BAND }); }} className="nav-link" href="#">
                        Bands
                      </a>
                    </li>
                  }

                  {this.state.user != null
                    ? <li className="nav-item"><a onClick={(e) => { e.preventDefault(); { this.QSetView({ page: LOGOUT }); this.QPostLogout(this); } }
                    } className="nav-link " href="#"> Logout </a>
                    </li>
                    : <li className="nav-item" ><a onClick={this.QSetView.bind(this, { page: LOGIN })}
                      className="nav-link " href="#"> Login </a>
                    </li>}
                </ul>
              </div>
            </div>
          </nav>
        </div>

        <div id="viewer" className="row container">
          {this.QGetView()}
        </div>
      </div>
    );
  }
}

export default App;
