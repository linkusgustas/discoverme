import React from "react";

class AboutView extends React.Component{

    render(){
        return(
        <div className="card" 
             style={{margin:"10px", width:"800px"}}>
            <div className="card-body">
                <h5 className="card-title">About us</h5>
                <p className="card-text">You are here to discover new bands
                in your area. Right now this project is not complete, thus 
                you can only discover bands, but not their records. 
                If you are an artist, you can also add your band and members.
                Login, go to the Bands page and see. Good luck!</p>
            </div>
        </div>
        )
    }
}

export default AboutView