import React from "react";
import PropTypes from 'prop-types';
import axios from "axios";
import { API_URL } from "../Utils/Configuration";

class BandView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Bands: [],
            showCreateForm: false,
            sortField: 'name',
            sortOrder: 'asc'
        };
    }

    componentDidMount() {
        axios.get(API_URL + '/bands')
            .then(response => {
                this.setState({ Bands: response.data })
            });
    }

    sortBands = (field, order) => {
        this.setState({ sortField: field, sortOrder: order });
    }

    getSortedBands = () => {
        const { Bands, sortField, sortOrder } = this.state;
        return Bands.sort((a, b) => {
            let comparison = 0;
            if (a[sortField] < b[sortField]) {
                comparison = -1;
            } else if (a[sortField] > b[sortField]) {
                comparison = 1;
            }
            return sortOrder === 'asc' ? comparison : -comparison;
        });
    }

    render() {
        const sortedBands = this.getSortedBands();
        const { isArtist, onViewDetailsClick } = this.props;
        return (
            <div>
                <div style={{ margin: '10px' }}>
                    <select onChange={(e) => this.sortBands(e.target.value, this.state.sortOrder)}>
                        <option value="name">Name</option>
                        <option value="country">Country</option>
                        <option value="city">City</option>
                        <option value="genre">Genre</option>
                        <option value="rating">Rating</option>
                    </select>
                    <button onClick={() => this.sortBands(this.state.sortField, this.state.sortOrder === 'asc' ? 'desc' : 'asc')}>
                        {this.state.sortOrder === 'asc' ? 'Ascending' : 'Descending'}
                    </button>
                </div>

                <div className="row row-cols-1 row-cols-md-2 g-4" style={{ margin: "10px" }}>
                    {sortedBands.length > 0 ? sortedBands.map((band) => (
                        <div className="col" key={band.id}>
                            <div className="card">
                                <div className="card-body">
                                    <h5 className="card-title">{band.name}</h5>
                                    <h6 className="card-subtitle mb-2 text-muted">Country: {band.country}</h6>
                                    <h6 className="card-subtitle mb-2 text-muted">City: {band.city}</h6>
                                    <h6 className="card-subtitle mb-2 text-muted">Genre: {band.genre}</h6>
                                    {band.rating !== null &&
                                        <h6 className="card-subtitle mb-2 text-muted">Rating: {band.rating}</h6>}
                                    <button onClick={() => onViewDetailsClick(band)} className="btn">
                                        View details
                                    </button>
                                </div>
                            </div>
                        </div>
                    )) : "No bands available"}
                </div>

                {isArtist === true &&
                    <button onClick={this.props.onAddBandClick} className="btn">
                        Create a New Band
                    </button>
                }
            </div>
        )
    }
}

BandView.propTypes = {
    isArtist: PropTypes.bool.isRequired
}

export default BandView;
