import React from "react";
import axios from "axios";
import { API_URL } from "../Utils/Configuration";

class AddBandView extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      band: {
        name: "",
        genre: "",
        country: "",
        city: "",
        firstRealeaseDate: "",
      },
      status: {
        success: null,
        msg: ""
      }
    }
  }
  
  QGetTextFromField = (e) => {
    const { name, value } = e.target;
    this.setState(prevState => ({
      band: {
        ...prevState.band,
        [name]: value
      }
    }));
  }

  QPostBand = () => {
    const bandData = {
      ...this.state.band,
      id_user: this.props.id_user
    };

    if (!bandData.name || !bandData.genre || !bandData.country || !bandData.city) {
      this.setState({ status: { success: false, msg: "All fields are required except the date" } });
      return;
    }

    if (!bandData.firstRealeaseDate) {
      bandData.firstRealeaseDate = null;
    }

    axios.post(API_URL + '/users/addBand', bandData, { withCredentials: true })
      .then(response => {
        if (response.status === 200) {
          this.setState({ status: { success: true, msg: "Band added successfully" } });
          this.props.onBandAdded();
        } else {
          this.setState({ status: { success: false, msg: "Adding failed" } });
        }
      })
      .catch(err => {
        console.log(err);
        this.setState({ status: { success: false, msg: "Error during adding of the band" } });
      });
  };



  render() {
    return (
      <div className="card"
        style={{ margin: "10px" }}>
        <h3 style={{ margin: "10px" }}>Add your new band</h3>
        <div className="mb-3"
          style={{ margin: "10px" }}>
          <label className="form-label">Name of the band</label>
          <input name="name" type="text"
            onChange={this.QGetTextFromField.bind(this)}
            className="form-control"
            placeholder="Name.." />
        </div>
        <div className="mb-3"
          style={{ margin: "10px" }}>
          <label className="form-label">Genre</label>
          <input name="genre" type="text" onChange={(e) => this.QGetTextFromField(e)}
            className="form-control"
            placeholder="Genre..." />
        </div>
        <div className="mb-3"
          style={{ margin: "10px" }}>
          <label className="form-label">Country</label>
          <input name="country" type="text" onChange={(e) => this.QGetTextFromField(e)}
            className="form-control"
            placeholder="Country..." />
        </div>
        <div className="mb-3"
          style={{ margin: "10px" }}>
          <label className="form-label">City</label>
          <input name="city" type="text" onChange={(e) => this.QGetTextFromField(e)}
            className="form-control"
            placeholder="City..." />
        </div>
        <div className="mb-3" style={{ margin: "10px" }}>
          <label className="form-label">Date of the First Release</label>
          <input
            name="firstRealeaseDate"
            type="date"
            onChange={(e) => this.QGetTextFromField(e)}
            className="form-control"
            placeholder="Date of the first release..."
          />
        </div>
        <button className="btn" onClick={() => this.QPostBand()}
          style={{ margin: "10px" }}>
          Add
        </button>
        {this.state.status.success ?
          <p className="alert alert-success"
            role="alert">{this.state.status.msg}</p> : null}

        {!this.state.status.success &&
          this.state.status.msg !== "" ?
          <p className="alert alert-danger"
            role="alert">{this.state.status.msg}</p> : null}
      </div>
    )
  }
}
export default AddBandView;