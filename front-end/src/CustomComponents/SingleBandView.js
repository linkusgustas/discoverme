import React from "react";
import axios from "axios";
import { API_URL } from "../Utils/Configuration";

class SingleBandView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Members: [],
            Records: []
        };
    }

    componentDidMount() {
        const { band } = this.props;
        console.log(band)
        if (band && band.id_band) {
            axios.get(`${API_URL}/bands/members/${band.id_band}`)
                .then(response => {
                    console.log(response.status);
                    this.setState({ Members: response.data });
                })
                .catch(error => {
                    console.log(error);
                });
        }
    }

    formatDate = (dateString) => {
        const date = new Date(dateString);
        return date.toISOString().split('T')[0];
    };
    
    renderMembers() {
        const { Members } = this.state;

        if (Members.length === 0) {
            return <h6>Band does not have any members yet.</h6>;
        }
        return Members.map(member => (
            <div key={member.id}>
                <h6>Name: {member.fullName}</h6>
                <h6>Position: {member.possition}</h6>
                <p></p>
            </div>
        ));
    }
    render() {
        const { band } = this.props;
        const { id_user } = this.props;
        return (
            <div className="card">
                <div className="card-body">
                    <h5 className="card-title">{band.name}</h5>
                    <h5 className="card-title">Country: {band.country}</h5>
                    <h5 className="card-title">City: {band.city}</h5>
                    <h5 className="card-title">Genre: {band.genre}</h5>
                    {band.rating !== null &&
                        <h5 className="card-title">Rating: {band.rating}</h5>}
                    {band.firstRealeaseDate &&
                        <h5 className="card-title" style={{ whiteSpace: 'nowrap' }}>
                            First release date: {this.formatDate(band.firstRealeaseDate)}
                        </h5>
                    }
                    <h5>Members:</h5>
                    {this.renderMembers()}
                    {band.fk_Userid_user === id_user &&
                        <button onClick={() => this.props.onAddMember(band)} className="btn btn-primary">
                            Add member
                        </button>
                    }
                </div>
            </div>
        );
    }
}
export default SingleBandView;