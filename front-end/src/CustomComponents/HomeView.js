import React from "react";

class HomeView extends React.Component {
    render() {
        const { user } = this.props;
        return (
            <div className="card" style={{ margin: "10px", width: "800px" }}>
                {console.log(user)}
                <div className="card-body">
                    <h5 className="card-title">Welcome!!!</h5>
                    {user
                        ? <p className="card-text">Welcome back, {user.fullName}!
                            Ready to explore more bands or add your own?</p>
                        : <p className="card-text">You are here to discover local
                            bands and add yours too! Please log in or
                            register to continue.</p>
                    }
                </div>
            </div>
        )
    }
}

export default HomeView