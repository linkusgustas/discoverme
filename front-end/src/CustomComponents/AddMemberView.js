import React from "react";
import axios from "axios";
import { API_URL } from "../Utils/Configuration";

class AddMemberView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            member: {
                fullName: "",
                possition: ""
            },
            status: {
                success: null,
                msg: ""
            }
        };
    }

    QGetTextFromField = (e) => {
        const { name, value } = e.target;
        this.setState(prevState => ({
            member: {
                ...prevState.member,
                [name]: value
            }
        }));
    }

    QPostMember = () => {
        const memberData = {
            ...this.state.member,
            id_band: this.props.id_band
        };

        if (!memberData.fullName || !memberData.possition) {
            this.setState({ status: { success: false, msg: "All fields are required!" } });
            return;
        }

        axios.post(API_URL + '/bands/addMember', memberData, { withCredentials: true })
            .then(response => {
                if (response.status === 200) {
                    this.setState({ status: { success: true, msg: "Member added successfully" } });
                    this.props.onMemberAdded();
                } else {
                    this.setState({ status: { success: false, msg: "Adding failed" } });
                }
            })
            .catch(err => {
                console.log(err);
                this.setState({ status: { success: false, msg: "Error during adding of the member" } });
            });

    }
    render() {
        return (
            <div className="card" style={{ margin: "10px" }}>
                <h3 style={{ margin: "10px" }}>Add new member</h3>
                <div className="mb-3"
                    style={{ margin: "10px" }}>
                    <label className="form-label">Full name of the member</label>
                    <input name="fullName" type="text"
                        onChange={this.QGetTextFromField.bind(this)}
                        className="form-control"
                        placeholder="Name.." />
                </div>
                <div className="mb-3"
                    style={{ margin: "10px" }}>
                    <label className="form-label">Playing position</label>
                    <input name="possition" type="text" onChange={(e) => this.QGetTextFromField(e)}
                        className="form-control"
                        placeholder="Playing possition..." />
                </div>
                <button className="btn" onClick={() => this.QPostMember()}
                    style={{ margin: "10px" }}>
                    Add member
                </button>
                {this.state.status.success ?
                    <p className="alert alert-success"
                        role="alert">{this.state.status.msg}</p> : null}

                {!this.state.status.success &&
                    this.state.status.msg !== "" ?
                    <p className="alert alert-danger"
                        role="alert">{this.state.status.msg}</p> : null}
            </div>

        );
    }
}

export default AddMemberView;
