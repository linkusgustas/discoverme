import React from "react";
import axios from "axios";
import './../App.css';
import { API_URL } from "../Utils/Configuration";

class RegisterView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {
        type: "signup",
        fullName: "",
        country: "",
        city: "",
        isArtist: false,
        username: "",
        email: "",
        password: ""
      },
      status: {
        success: null,
        msg: ""
      }
    };
  }


  QGetTextFromField = (e) => {
    this.setState((prevState) => ({
      user: { ...prevState.user, [e.target.name]: e.target.value },
    }));
  };

  QHandleCheckboxChange = (e) => {
    this.setState((prevState) => ({
      user: { ...prevState.user, isArtist: e.target.checked ? 1 : 0 },
    }));
  };
  validateEmail = (email) => {
    const re = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
    return re.test(String(email).toLowerCase());
  }

  QPostSignup = () => {
    console.log(this.state.user.email)
    if (!this.validateEmail(this.state.user.email)) {
      this.setState({
        status: {
          success: false,
          msg: "Invalid email format"
        }
      });
      return;
    }
    axios.post(API_URL + "/users/register", this.state.user)
      .then((response) => {
        this.setState({
          status: {
            success: true,
            msg: "Registration successful!"
          }
        });
        console.log("Sent to server...");
        if (this.props.onRegisterSuccess) {
          this.props.onRegisterSuccess();
        }
      })
      .catch((error) => {
        let msg = "Registration failed!";
        if (error.response) {
          msg = error.response.data;
        }
        this.setState({
          status: {
            success: false,
            msg: msg
          }
        });
        console.log(error);
      });
  };


  render() {
    return (
      <div
        className="card"
        style={{
          width: "400px",
          marginLeft: "auto",
          marginRight: "auto",
          marginTop: "10px",
          marginBottom: "10px",
        }}
      >
        <form style={{ margin: "20px" }}>
          <div className="mb-3">
            <label className="form-label">Full Name</label>
            <input
              onChange={(e) => this.QGetTextFromField(e)}
              name="fullName"
              type="text"
              className="form-control"
            />
          </div>
          <div className="mb-3">
            <label className="form-label">Country</label>
            <input
              onChange={(e) => this.QGetTextFromField(e)}
              name="country"
              type="text"
              className="form-control"
            />
          </div>
          <div className="mb-3">
            <label className="form-label">City</label>
            <input
              onChange={(e) => this.QGetTextFromField(e)}
              name="city"
              type="text"
              className="form-control"
            />
          </div>
          <div className="mb-3">
            <label className="form-label">Are you an artist?</label>
            <input
              onChange={this.QHandleCheckboxChange}
              name="isArtist"
              type="checkbox"
              className="form-check-input"
            />
          </div>
          <div className="mb-3">
            <label className="form-label">Username</label>
            <input
              onChange={(e) => this.QGetTextFromField(e)}
              name="username"
              type="text"
              className="form-control"
            />
          </div>
          <div className="mb-3">
            <label className="form-label">Email address</label>
            <input
              onChange={(e) => this.QGetTextFromField(e)}
              name="email"
              type="email"
              className="form-control"
            />
          </div>
          <div className="mb-3">
            <label className="form-label">Password</label>
            <input
              onChange={(e) => this.QGetTextFromField(e)}
              name="password"
              type="password"
              className="form-control"
            />
          </div>
        </form>
        <button
          onClick={() => this.QPostSignup()}
          style={{ margin: "10px" }}
          className="btn"
        >
          Submit
        </button>
        {this.state.status.success ?
          <p className="alert alert-success"
            role="alert">{this.state.status.msg}</p> : null}

        {!this.state.status.success &&
          this.state.status.msg !== "" ?
          <p className="alert alert-danger"
            role="alert">{this.state.status.msg}</p> : null}

      </div>
    );
  }
}

export default RegisterView;
