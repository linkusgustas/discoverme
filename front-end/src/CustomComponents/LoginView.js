import React from "react";
import PropTypes from 'prop-types';
import axios from "axios";
import { API_URL } from "../Utils/Configuration";

class LoginView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user_input: {
        username: "",
        password: ""
      },
      status: {
        success: null,
        msg: ""
      }
    }
  }

  QGetTextFromField = (e) => {
    this.setState({
      user_input: { ...this.state.user_input, [e.target.name]: e.target.value }
    });
  }

  QPostLogin = () => {
    if (!this.state.user_input.username || !this.state.user_input.password) {
      this.setState({ status: { success: false, msg: "Missing input field" } })
      ;
      return;
    }

    axios.post(API_URL + '/users/login', this.state.user_input, { withCredentials: true })
      .then(response => {
        if (response.status === 200 && response.data.logged) {
          this.props.QUserFromChild(response.data.user, true);
          if (this.props.onLoginSuccess) {
            this.props.onLoginSuccess();
        }
        } else {
          this.setState({ status: { success: false, msg: "Login failed" } });
        }
      })
      .catch(err => {
        console.log(err);
        this.setState({ status: { success: false, msg: "Error during login" } });
      });
  }

  render() {
    const { registrationSuccessMessage } = this.props;
    return (
      <div className="card"
        style={{ width: "400px", marginLeft: "auto", marginRight: "auto", marginTop: "10px", marginBottom: "10px" }}>
        <form style={{ margin: "20px" }} >
          <div className="mb-3">
            <label className="form-label">Username</label>
            <input name="username" onChange={(e) => this.QGetTextFromField(e)}
              type="text"
              className="form-control"
              id="exampleInputEmail1" />
          </div>
          <div className="mb-3">
            <label className="form-label">Password</label>
            <input name="password" onChange={(e) => this.QGetTextFromField(e)}
              type="password"
              className="form-control"
              id="exampleInputPassword1" />
          </div>
        </form>
        <button style={{ margin: "10px" }} onClick={() => this.QPostLogin()}
          className="btn">Log in</button>
        {registrationSuccessMessage &&
          <p className="alert alert-success" role="alert">{registrationSuccessMessage}</p>
        }
        {this.state.status.success ?
          <p className="alert alert-success"
            role="alert">{this.state.status.msg}</p> : null}

        {!this.state.status.success &&
          this.state.status.msg !== "" ?
          <p className="alert alert-danger"
            role="alert">{this.state.status.msg}</p> : null}

      </div>
    )
  }
}

LoginView.propTypes = {
  QUserFromChild: PropTypes.func.isRequired,
  registrationSuccessMessage: PropTypes.string,
  onLoginSuccess: PropTypes.func,
};



export default LoginView