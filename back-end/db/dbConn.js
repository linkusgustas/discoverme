const mysql = require('mysql');

const conn = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_DATABASE,
})

conn.connect((err) => {
  if (err) {
    console.log("ERROR: " + err.message);
    return;
  }
  console.log('Connection established');
})
let dataPool = {}


module.exports = dataPool;

dataPool.allBands = () => {
  return new Promise((resolve, reject) => {
    conn.query(`SELECT * FROM Band`, (err, res) => {
      if (err) { return reject(err) }
      return resolve(res)
    })
  })
}
dataPool.getMembersByBandId = (id_band) => {
  return new Promise((resolve, reject) => {
    conn.query('SELECT * FROM Member WHERE fk_Bandid_band = ?', [id_band], (err, res, fields) => {
      if (err) { return reject(err) }
      return resolve(res)
    });
  });
};

dataPool.AddMembers = (fullName, possition, id_band) => {
  return new Promise((resolve, reject) => {
    conn.query(
      `INSERT INTO Member (fullName, possition, fk_Bandid_band) VALUES (?, ?, ?)`,
      [fullName, possition, id_band],
      (err, res) => {
        if (err) {
          console.log(err.message);
          return reject(err);
        }
        return resolve(res);
      }
    );
  });
};


dataPool.AuthUser = (username) => {
  return new Promise((resolve, reject) => {
    conn.query('SELECT * FROM User WHERE username = ?', username, (err, res, fields) => {
      if (err) { return reject(err) }
      console.log(dataPool)
      return resolve(res)
    })
  })
}

dataPool.AddBand = (name, genre, country, city, firstRealeaseDate, id_user) => {
  return new Promise((resolve, reject) => {
    conn.query(
      `INSERT INTO Band (name, genre, country, city, firstRealeaseDate, fk_Userid_user) VALUES (?, ?, ?, ?, ?, ?)`,
      [name, genre, country, city, firstRealeaseDate, id_user],
      (err, res) => {
        if (err) {
          console.log(err.message)
          return reject(err);
        }
        return resolve(res);
      }
    );
  })

}

dataPool.AddUser = (username, email, password, fullName, country, city, isArtist) => {
  return new Promise((resolve, reject) => {
    conn.query(
      `SELECT * FROM User WHERE username = ? OR email = ?`,
      [username, email],
      (err, res) => {
        if (err) {
          return reject(err);
        }
        if (res.length > 0) {
          return reject(new Error("User already exists"));
        } else {
          conn.query(
            `INSERT INTO User (username, email, password, fullName, country, city, isArtist) VALUES (?, ?, ?, ?, ?, ?, ?)`,
            [username, email, password, fullName, country, city, isArtist],
            (err, res) => {
              if (err) {
                return reject(err);
              }
              return resolve(res);
            }
          );
        }
      }
    );
  });
};

