const express = require('express')
const bands = express.Router()
const DB = require("../db/dbConn.js")

bands.get('/', async (req, res, next) => {
    try {
        var queryResult = await DB.allBands();
        res.json(queryResult)
    }
    catch (err) {
        console.log(err)
        res.sendStatus(500)
        next()
    }
})
bands.get('/members/:id_band', async (req, res, next) => {
    try {
        const bandId = req.params.id_band;
        var queryResult = await DB.getMembersByBandId(bandId);
        res.json(queryResult);
    } catch (err) {
        console.log(err);
        res.sendStatus(500);
        next();
    }
});

bands.post('/addMember', async (req, res, next) => {
    try {
        let fullName = req.body.fullName;
        let possition = req.body.possition;
        let id_band = req.body.id_band;
        console.log(fullName)
        console.log(possition)
        console.log(id_band)
        
        var isAcompleteMember = fullName && possition && id_band;
        if (isAcompleteMember) {
            var queryResult = await DB.AddMembers(fullName, possition, id_band);
            if (queryResult.affectedRows) {
                console.log("New member added");
                res.json({ success: true, message: "New member added" });
            } else {
                throw new Error("Member could not be added");
            }
        } else {
            console.log("A field is empty!!");
            res.status(400).json({ success: false, message: "All fields are required" });
        }
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server Error");
    }
});

bands.get('/:id', async (req, res, next) => {
    try {
        var queryResult = await DB.oneNovica(req.params.id)
        res.json(queryResult)
    }
    catch (err) {
        console.log(err)
        res.sendStatus(500)
        next()
    }
})
module.exports = bands
