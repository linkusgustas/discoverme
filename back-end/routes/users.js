const express = require("express")
const users = express.Router();
const DB = require('../db/dbConn.js')
const bcrypt = require('bcrypt');
const saltRounds = 10; // This is the cost factor for hashing

users.post('/login', async (req, res) => {
    try {
        const { username, password } = req.body;

        if (username && password) {
            const userResult = await DB.AuthUser(username);
            if (userResult.length > 0) {
                const match = await bcrypt.compare(password, userResult[0].password);
                if (match) {
                    console.log("User logged in successfully");
                    req.session.user = userResult[0];
                    req.session.logged_in = true;

                    res.send({ logged: true, user: userResult[0], status: { success: true, msg: "Logged in" } });
                } else {
                    console.log("Incorrect password");
                    res.status(401).send({ status: { success: false, msg: "Incorrect username or password" } });
                }
            } else {
                console.log("User not registered");
                res.status(401).send({ status: { success: false, msg: "Incorrect username or password" } });
            }
        } else {
            console.log("Please enter Username and Password");
            res.status(400).send({ status: { success: false, msg: "Please enter Username and Password" } });
        }
    } catch (err) {
        console.log(err);
        res.status(500).send({ status: { success: false, msg: "Server error occurred" } });
    }
});

users.get('/session', async (req, res, next) => {
    try {
        res.json(req.session)
    } catch (error) {
        res.sendStatus(500)
    }
})

users.get('/logout', async (req, res, next) => {
    try {
        req.session.destroy()
        console.log("Session destoryed")
        res.json({ success: true, msg: "Session destoryed" })
    } catch (error) {
        res.sendStatus(500)
    }
})

users.post('/addBand', async (req, res) => {
    let { name, genre, country, city, firstRealeaseDate, id_user } = req.body;
    console.log(req.body)
    if (name && genre && city && country) {
        try {
            let queryResult = await DB.AddBand(name, genre, country, city, firstRealeaseDate, id_user);

            if (queryResult.affectedRows) {
                console.log("Band added");
                res.status(200).send("Band was added");

            }
            else {
                res.status(400).send("Adding band failed");
            }
        }
        catch (err) {
            console.log(err.message);
        }
    }
    else {
        console.log("A field is missing!");
        res.status(400).send("Required fields are missing");
    }

})

users.post('/register', async (req, res) => {
    let { username, password, email, fullName, city, country, isArtist } = req.body;

    if (username && password && email && city && country && fullName) {
        try {
            const hashedPassword = await bcrypt.hash(password, saltRounds);

            let queryResult = await DB.AddUser(username, email, hashedPassword, fullName, country, city, isArtist);

            if (queryResult.affectedRows) {
                console.log("New user added!!");
                res.status(201).send("User registered successfully");
            } else {
                res.status(400).send("User registration failed");
            }
        } catch (err) {
            if (err.message === "User already exists") {
                res.status(409).send("User already exists. Please try a different username or email.");
            } else {
                console.log(err);
                res.status(500).send("Server error occurred. Please try again later.");
            }
        }
    } else {
        console.log("A field is missing!");
        res.status(400).send("Required fields are missing");
    }
});


module.exports = users
