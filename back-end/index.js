const express = require("express")
const session = require('express-session');
require("dotenv").config()
const cookieParser = require('cookie-parser'); 
const app = express()
const cors = require("cors")
const port = process.env.PORT || 8147
//Some configurations
app.use(express.json())
app.use(express.urlencoded({extended : true}));
app.use(cors({
  methods:["GET", "POST"],
  origin: 'http://localhost:3000',
  credentials: true
}))

app.use(cookieParser('your-secret'));
// // set a cookie
app.use(function (req, res, next) {
  // check if client sent cookie
  var cookie = req.cookies.cookieName;
  if (cookie === undefined) {
    // no: set a new cookie
    var randomNumber=Math.random().toString();
    randomNumber=randomNumber.substring(2,randomNumber.length);
    res.cookie('cookieName',randomNumber, { maxAge: 900000, httpOnly: true });
    console.log('cookie created successfully');
  } else {
    // yes, cookie was already present 
    console.log('cookie exists', cookie);
  } 
  next(); // <-- important!
});

let sess = {
  secret: 'our litle secrett',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false, sameSite: 'None' }
}
app.use(session(sess))

const users = require("./routes/users")  
const bands = require("./routes/bands")
app.use("/users", users)
app.use("/bands", bands)

const path = require('path')
app.use(express.static(path.join(__dirname, "build")))
app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname, "build", "index.html")) 
})

app.listen(port, ()=>{
console.log(`Server is running on port: ${port}`)
})
